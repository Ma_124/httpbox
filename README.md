# HttpBox
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/Ma_124/httpbox)](https://goreportcard.com/report/gitlab.com/Ma_124/httpbox)
[![CodeCov](https://codecov.io/gl/Ma_124/httpbox/branch/master/graph/badge.svg)](https://codecov.io/gl/Ma_124/httpbox)
[![Build Status](https://gitlab.com/Ma_124/httpbox/badges/master/build.svg)](https://gitlab.com/Ma_124/httpbox/commits/master)
[![GoDoc](https://godoc.org/gitlab.com/Ma_124/httpbox?status.svg)](https://godoc.org/gitlab.com/Ma_124/httpbox)
[![License MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://ma124.js.org/l/MIT/~/2019)

HttpBox is a sandboxing library which can check whether a `*http.Request` or an `*url.URL` conform to a specific policy.

# Example

```go
package main

import (
    "fmt"
    . "gitlab.com/Ma_124/httpbox"
    "net/http"
    "regexp"
)

func main() {
	proxy := &TripperProxy{
		RoundTripper: http.DefaultTransport,
		Policy:       &RequestPolicyImpl{
			AllowedMethods: map[string]struct{}{
				"GET": {},
			},
			ForbidBody:        true,
			OverrideClose:     LeaveUnmodified,
			URLPolicy:         OrURLPolicy{
				RegexURLPolicy{regexp.MustCompile(`^https?://(www\.)?example\.(org|com)$`)},
				LiteralURLWhitelistPolicy{
					"https://gitlab.com/Ma_124/httpbox": {},
					"https://gitlab.com/Ma_124/errutils": {},
				},
			},
			OverrideUserAgent: "",
		},
	}

	client := &http.Client{
		Transport:     proxy,
	}

	doRequest := func(url string) {
		resp, err := client.Get(url)
		if err != nil {
			fmt.Printf("%s: failed: %s\n", url, err)
		} else {
			fmt.Printf("%s: success: %d\n", url, resp.StatusCode)
			_ = resp.Body.Close()
		}
	}

	doRequest("http://example.com")
	doRequest("https://www.example.org")
	doRequest("https://gitlab.com/Ma_124/httpbox")
	doRequest("https://gitlab.com/Ma_124/errutils")
	fmt.Println()
	doRequest("https://google.com")
	// Output:
	// http://example.com: success: 200
	// https://www.example.org: success: 200
	// https://gitlab.com/Ma_124/httpbox: success: 200
	// https://gitlab.com/Ma_124/errutils: success: 200
	//
	// https://google.com: failed: Get https://google.com: multiple errors occurred: [0x650F: NotMatchRegexp, 0x4E1E: NotWhitelisted]
}
```
