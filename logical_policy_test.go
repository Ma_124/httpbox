package httpbox

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestLogicalRequestPolicies(t *testing.T) {
	testLogicalRequestPolicy(t, "true", TrueRequestPolicy, true)
	testLogicalRequestPolicy(t, "false", FalseRequestPolicy, false)

	testLogicalRequestPolicy(t, "and t", AndRequestPolicy{TrueRequestPolicy}, true)
	testLogicalRequestPolicy(t, "and f", AndRequestPolicy{FalseRequestPolicy}, false)
	testLogicalRequestPolicy(t, "and fff", AndRequestPolicy{FalseRequestPolicy, FalseRequestPolicy, FalseRequestPolicy}, false)
	testLogicalRequestPolicy(t, "and tft", AndRequestPolicy{TrueRequestPolicy, FalseRequestPolicy, TrueRequestPolicy}, false)
	testLogicalRequestPolicy(t, "and ttt", AndRequestPolicy{TrueRequestPolicy, TrueRequestPolicy, TrueRequestPolicy}, true)

	testLogicalRequestPolicy(t, "or t", OrRequestPolicy{TrueRequestPolicy}, true)
	testLogicalRequestPolicy(t, "or f", OrRequestPolicy{FalseRequestPolicy}, false)
	testLogicalRequestPolicy(t, "or fff", OrRequestPolicy{FalseRequestPolicy, FalseRequestPolicy, FalseRequestPolicy}, false)
	testLogicalRequestPolicy(t, "or tft", OrRequestPolicy{TrueRequestPolicy, FalseRequestPolicy, TrueRequestPolicy}, true)
	testLogicalRequestPolicy(t, "or ttt", OrRequestPolicy{TrueRequestPolicy, TrueRequestPolicy, TrueRequestPolicy}, true)

	testLogicalRequestPolicy(t, "not t", NotRequestPolicy{TrueRequestPolicy, nil}, false)
	testLogicalRequestPolicy(t, "not f", NotRequestPolicy{FalseRequestPolicy, nil}, true)
	t.Run("not t with reason", func(t *testing.T) {
		assert.Equal(t, NotRequestPolicy{TrueRequestPolicy, ErrNotWhitelisted}.CheckRequest(nil), ErrNotWhitelisted)
	})
}

func testLogicalRequestPolicy(t *testing.T, name string, p RequestPolicy, v bool) {
	t.Run(name, func(t *testing.T) {
		assert.Equal(t, p.CheckRequest(nil) == nil, v)
	})
}

func TestLogicalURLPolicies(t *testing.T) {
	testLogicalURLPolicy(t, "true", TrueURLPolicy, true)
	testLogicalURLPolicy(t, "false", FalseURLPolicy, false)

	testLogicalURLPolicy(t, "and t", AndURLPolicy{TrueURLPolicy}, true)
	testLogicalURLPolicy(t, "and f", AndURLPolicy{FalseURLPolicy}, false)
	testLogicalURLPolicy(t, "and fff", AndURLPolicy{FalseURLPolicy, FalseURLPolicy, FalseURLPolicy}, false)
	testLogicalURLPolicy(t, "and tft", AndURLPolicy{TrueURLPolicy, FalseURLPolicy, TrueURLPolicy}, false)
	testLogicalURLPolicy(t, "and ttt", AndURLPolicy{TrueURLPolicy, TrueURLPolicy, TrueURLPolicy}, true)

	testLogicalURLPolicy(t, "or t", OrURLPolicy{TrueURLPolicy}, true)
	testLogicalURLPolicy(t, "or f", OrURLPolicy{FalseURLPolicy}, false)
	testLogicalURLPolicy(t, "or fff", OrURLPolicy{FalseURLPolicy, FalseURLPolicy, FalseURLPolicy}, false)
	testLogicalURLPolicy(t, "or tft", OrURLPolicy{TrueURLPolicy, FalseURLPolicy, TrueURLPolicy}, true)
	testLogicalURLPolicy(t, "or ttt", OrURLPolicy{TrueURLPolicy, TrueURLPolicy, TrueURLPolicy}, true)

	testLogicalURLPolicy(t, "not t", NotURLPolicy{TrueURLPolicy, nil}, false)
	testLogicalURLPolicy(t, "not f", NotURLPolicy{FalseURLPolicy, nil}, true)
	t.Run("not t with reason", func(t *testing.T) {
		assert.Equal(t, NotURLPolicy{TrueURLPolicy, ErrNotWhitelisted}.CheckURL(nil), ErrNotWhitelisted)
	})
}

func testLogicalURLPolicy(t *testing.T, name string, p URLPolicy, v bool) {
	t.Run(name, func(t *testing.T) {
		assert.Equal(t, p.CheckURL(nil) == nil, v)
	})
}
