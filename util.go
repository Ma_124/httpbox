package httpbox

// OverrideBoolValue holds the whether a bool should be overridden and if that is the case with which value
// So there are 3 possible values: LeaveUnmodified, OverrideWithTrue, OverrideWithFalse
type OverrideBoolValue uint8

const (
	// LeaveUnmodified does not modify the bool variable
	LeaveUnmodified OverrideBoolValue = iota

	// OverrideWithTrue overrides the bool variable with true
	OverrideWithTrue

	// OverrideWithFalse overrides the bool variable with false
	OverrideWithFalse
)
