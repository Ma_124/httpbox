package httpbox

import (
	"net/http"
	"net/url"
	"strings"
)

// RequestPolicyImpl implements RequestPolicy
type RequestPolicyImpl struct {
	// AllowedMethods is a map of allowed methods
	// All method names (map keys) must be uppercase and all values must be empty structs.
	// For example the following map only allows GET and HEAD requests.
	//     map[string]struct{}{
	//         "GET": {},
	//         "HEAD": {},
	//     }
	AllowedMethods map[string]struct{}

	// ForbidBody determines whether a request body is allowed
	ForbidBody bool

	// OverrideClose overrides req.Close according the rules of OverrideBoolValue
	OverrideClose OverrideBoolValue

	// URLPolicy is used to check req.Host and req.URL; if it is nil all urls are allowed
	URLPolicy URLPolicy

	// OverrideUserAgent overrides the User-Agent header of the request if OverrideUserAgent != ""
	OverrideUserAgent string

	// TODO headers, trailers, cookies
}

// CheckRequest checks whether the given *http.Request conforms with this policy
func (p *RequestPolicyImpl) CheckRequest(req *http.Request) error {
	if p.AllowedMethods != nil {
		if _, ok := p.AllowedMethods[strings.ToUpper(req.Method)]; !ok {
			return ErrIllegalMethod
		}
	}

	if p.ForbidBody && req.Body != nil {
		_ = req.Body.Close()
		return ErrIllegalBody
	}

	if p.OverrideClose != LeaveUnmodified {
		req.Close = p.OverrideClose == OverrideWithTrue
	}

	// req.Host overrides req.URL.Host
	if req.Host != "" {
		req.URL.Host = req.Host
	}

	req.RequestURI = ""

	if err := p.CheckURL(req.URL); err != nil {
		return err
	}

	if p.OverrideUserAgent != "" {
		if req.Header != nil {
			req.Header.Set("User-Agent", p.OverrideUserAgent)
		} else {
			req.Header = map[string][]string{"User-Agent": {p.OverrideUserAgent}}
		}
	}

	return nil
}

// CheckURL checks whether the given *url.URL conforms with p.URLPolicy
// It returns true if p.URLPolicy == nil
func (p *RequestPolicyImpl) CheckURL(u *url.URL) error {
	if p.URLPolicy != nil {
		return p.URLPolicy.CheckURL(u)
	}
	return nil
}
