// Code generated by errgen at 03 Aug 19 13:16 CEST DO NOT EDIT.
// See https://gitlab.com/Ma_124/errutils for details.

// Copyright 2019 Ma_124, ma124.js.org <ma_124+oss@pm.me>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package httpbox

import (
	"gitlab.com/Ma_124/errutils"
)

type httpboxError uint16

const (
	// Unknown (0x0000)
	ErrUnknown httpboxError = 0x0000

	// requests with this method are not allowed (IllegalMethod: 0x9986)
	ErrIllegalMethod httpboxError = 0x9986

	// requests with bodys are not allowed (IllegalBody: 0x33A7)
	ErrIllegalBody httpboxError = 0x33A7

	// request is not allowed (NotPolicy: 0x2E22)
	ErrNotPolicy httpboxError = 0x2E22

	// request is not allowed (NotMatchRegexp: 0x650F)
	ErrNotMatchRegexp httpboxError = 0x650F

	// url is not whitelisted (NotWhitelisted: 0x4E1E)
	ErrNotWhitelisted httpboxError = 0x4E1E

	// url is blacklisted (Blacklisted: 0x8E58)
	ErrBlacklisted httpboxError = 0x8E58
)

func GetErrByName(name string) errutils.ExtErrorWithID {
	switch name {

	// requests with this method are not allowed (IllegalMethod: 0x9986)
	case "IllegalMethod":
		return ErrIllegalMethod

	// requests with bodys are not allowed (IllegalBody: 0x33A7)
	case "IllegalBody":
		return ErrIllegalBody

	// request is not allowed (NotPolicy: 0x2E22)
	case "NotPolicy":
		return ErrNotPolicy

	// request is not allowed (NotMatchRegexp: 0x650F)
	case "NotMatchRegexp":
		return ErrNotMatchRegexp

	// url is not whitelisted (NotWhitelisted: 0x4E1E)
	case "NotWhitelisted":
		return ErrNotWhitelisted

	// url is blacklisted (Blacklisted: 0x8E58)
	case "Blacklisted":
		return ErrBlacklisted

	default:
		return ErrUnknown
	}
}

func (err httpboxError) Error() string {

	return errutils.ErrorMsg(err)

}

func (err httpboxError) ID() (string, uint16) {
	return "httpbox", uint16(err)
}

func (err httpboxError) Message() string {
	switch err {

	// requests with this method are not allowed (IllegalMethod: 0x9986)
	case ErrIllegalMethod:
		return "IllegalMethod"

	// requests with bodys are not allowed (IllegalBody: 0x33A7)
	case ErrIllegalBody:
		return "IllegalBody"

	// request is not allowed (NotPolicy: 0x2E22)
	case ErrNotPolicy:
		return "NotPolicy"

	// request is not allowed (NotMatchRegexp: 0x650F)
	case ErrNotMatchRegexp:
		return "NotMatchRegexp"

	// url is not whitelisted (NotWhitelisted: 0x4E1E)
	case ErrNotWhitelisted:
		return "NotWhitelisted"

	// url is blacklisted (Blacklisted: 0x8E58)
	case ErrBlacklisted:
		return "Blacklisted"

	default:
		return "Unknown"
	}
}

func (err httpboxError) Cause() error {
	return nil
}

func (err httpboxError) IsWarn() bool {
	switch err {

	default:
		return false
	}
}
