package httpbox

import (
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"strings"
	"testing"
)

type RoundTripperFunc func()

func (f RoundTripperFunc) RoundTrip(req *http.Request) (*http.Response, error) {
	f()
	return nil, nil
}

func TestTripperProxy_RoundTrip(t *testing.T) {
	tests := []struct {
		name       string
		policy     RequestPolicy
		req        *http.Request
		wantCalled bool
	}{
		{
			name:       "basic",
			policy:     &RequestPolicyImpl{},
			req:        &http.Request{},
			wantCalled: true,
		},
		{
			name:       "nil",
			policy:     nil,
			req:        &http.Request{},
			wantCalled: true,
		},
		{
			name: "forbid",
			policy: &RequestPolicyImpl{
				ForbidBody: true,
			},
			req: &http.Request{
				Body: ioutil.NopCloser(strings.NewReader("body text")),
			},
			wantCalled: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			called := false
			p := &TripperProxy{
				RoundTripper: RoundTripperFunc(func() {
					called = true
				}),
				Policy: tt.policy,
			}

			_, _ = p.RoundTrip(tt.req)

			assert.Equal(t, tt.wantCalled, called)
		})
	}

}
