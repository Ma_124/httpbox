package httpbox

import (
	"fmt"
	"net/http"
	"regexp"
)

func ExampleTripperProxy() {
	proxy := &TripperProxy{
		RoundTripper: http.DefaultTransport,
		Policy: &RequestPolicyImpl{
			AllowedMethods: map[string]struct{}{
				"GET": {},
			},
			ForbidBody:    true,
			OverrideClose: LeaveUnmodified,
			URLPolicy: OrURLPolicy{
				RegexURLPolicy{regexp.MustCompile(`^https?://(www\.)?example\.(org|com)$`)},
				LiteralURLWhitelistPolicy{
					"https://gitlab.com/Ma_124/httpbox":  {},
					"https://gitlab.com/Ma_124/errutils": {},
				},
			},
			OverrideUserAgent: "",
		},
	}

	client := &http.Client{
		Transport: proxy,
	}

	doRequest := func(url string) {
		resp, err := client.Get(url)
		if err != nil {
			fmt.Printf("%s: failed: %s\n", url, err)
		} else {
			fmt.Printf("%s: success: %d\n", url, resp.StatusCode)
			_ = resp.Body.Close()
		}
	}

	doRequest("http://example.com")
	doRequest("https://www.example.org")
	doRequest("https://gitlab.com/Ma_124/httpbox")
	doRequest("https://gitlab.com/Ma_124/errutils")
	fmt.Println()
	doRequest("https://google.com")
	// Output:
	// http://example.com: success: 200
	// https://www.example.org: success: 200
	// https://gitlab.com/Ma_124/httpbox: success: 200
	// https://gitlab.com/Ma_124/errutils: success: 200
	//
	// https://google.com: failed: Get https://google.com: multiple errors occurred: [0x650F: NotMatchRegexp, 0x4E1E: NotWhitelisted]
}
