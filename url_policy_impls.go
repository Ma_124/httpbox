package httpbox

import (
	"net/url"
	"regexp"
)

// LiteralURLWhitelistPolicy is an URLPolicy that looks url.String() in the underlying map up and if that key exists allows the url
// NOTE: a whitelist containing example.org forbids example.org?query and example.org#fragment
type LiteralURLWhitelistPolicy map[string]struct{}

// CheckURL looks u.String() in the underlying map up and if that key exists allows the url
func (wl LiteralURLWhitelistPolicy) CheckURL(u *url.URL) error {
	_, ok := wl[u.String()]
	if ok {
		return nil
	}
	return ErrNotWhitelisted
}

// there is no literal blacklist because a blacklist containing example.org would allow example.org?query and example.org#fragment
// which is a potential security risk if the developer is not careful. Use NotURLPolicy{LiteralURLWhitelistPolicy(...), ErrBlacklisted} instead (which has the same problem)

// RegexURLPolicy is an URLPolicy that allows only URLs matching Regexp
// NOTE: Regexp must start with a circumflex (^) and end with a dollar sign ($)
type RegexURLPolicy struct {
	Regexp *regexp.Regexp
}

// CheckURL tries to match Regexp with u.String() and only allows the URL if it is successful
func (r RegexURLPolicy) CheckURL(u *url.URL) error {
	if !r.Regexp.MatchString(u.String()) {
		return ErrNotMatchRegexp
	}
	expr := r.Regexp.String()
	if len(expr) == 0 || expr[0] != '^' || expr[len(expr)-1] != '$' {
		return ErrNotMatchRegexp // this specific error value here should not be considered part of API; may change in the future
	}
	return nil
}
