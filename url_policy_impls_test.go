package httpbox

import (
	"github.com/stretchr/testify/assert"
	"net/url"
	"regexp"
	"testing"
)

func TestLiteralURLWhitelistPolicy_CheckURL(t *testing.T) {
	tests := []struct {
		name string
		wl   LiteralURLWhitelistPolicy
		u    *url.URL
		err  error
	}{
		{
			name: "in whitelist",
			wl:   LiteralURLWhitelistPolicy{"https://example.org": {}},
			u:    MustParseURL("https://example.org"),
		},
		{
			name: "not in whitelist",
			wl:   LiteralURLWhitelistPolicy{"https://example.org": {}},
			u:    MustParseURL("https://gitlab.com"),
			err:  ErrNotWhitelisted,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := tt.wl.CheckURL(tt.u)

			if tt.err != nil && err != nil {
				assert.Equal(t, tt.err.Error(), err.Error())
			} else {
				assert.Equal(t, tt.err, err)
			}
		})
	}
}

func TestRegexURLPolicy_CheckURL(t *testing.T) {
	tests := []struct {
		name   string
		regexp string
		u      *url.URL
		err    error
	}{
		{
			name:   "match regex",
			regexp: `^https://example\.(org|com)$`,
			u:      MustParseURL("https://example.org"),
			err:    nil,
		},
		{
			name:   "match regex",
			regexp: `^https://gitlab\.com$`,
			u:      MustParseURL("https://example.org"),
			err:    ErrNotMatchRegexp,
		},
		{
			name:   "regex zero len",
			regexp: ``,
			u:      MustParseURL("https://example.org"),
			err:    ErrNotMatchRegexp, // this specific error value here should not be considered part of API; may change in the future
		},
		{
			name:   "regex no circumflex",
			regexp: `https://gitlab\.com$`,
			u:      MustParseURL("https://example.org"),
			err:    ErrNotMatchRegexp, // this specific error value here should not be considered part of API; may change in the future
		},
		{
			name:   "regex no dollar",
			regexp: `^https://gitlab\.com`,
			u:      MustParseURL("https://example.org"),
			err:    ErrNotMatchRegexp, // this specific error value here should not be considered part of API; may change in the future
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := RegexURLPolicy{
				Regexp: regexp.MustCompile(tt.regexp),
			}

			err := r.CheckURL(tt.u)

			if tt.err != nil && err != nil {
				assert.Equal(t, tt.err.Error(), err.Error())
			} else {
				assert.Equal(t, tt.err, err)
			}
		})
	}
}
