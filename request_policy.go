package httpbox

import (
	"gitlab.com/Ma_124/errutils"
	"net/http"
)

// A RequestPolicy restricts HTTP requests allowed by CheckRequest
type RequestPolicy interface {
	// CheckRequest checks whether the given *http.Request conforms to this policy
	CheckRequest(req *http.Request) error
}

// RequestPolicyFunc is an implementation of RequestPolicy which calls itself
type RequestPolicyFunc func(req *http.Request) error

// CheckRequest is a wrapper around f
func (f RequestPolicyFunc) CheckRequest(req *http.Request) error {
	return f(req)
}

// TrueRequestPolicy allows all requests
var TrueRequestPolicy = RequestPolicyFunc(func(u *http.Request) error {
	return nil
})

// FalseRequestPolicy forbids all requests
var FalseRequestPolicy = RequestPolicyFunc(func(u *http.Request) error {
	return ErrNotPolicy
})

// AndRequestPolicy is a logical AND of multiple RequestPolicies
type AndRequestPolicy []RequestPolicy

// CheckRequest performs a logical AND on the results of the underlying RequestPolicies
func (ps AndRequestPolicy) CheckRequest(req *http.Request) error {
	for _, p := range ps {
		if err := p.CheckRequest(req); err != nil {
			return err
		}
	}
	return nil
}

// OrRequestPolicy is a logical OR of multiple RequestPolicies
type OrRequestPolicy []RequestPolicy

// CheckRequest performs a logical OR on the results of the underlying RequestPolicies
func (ps OrRequestPolicy) CheckRequest(req *http.Request) error {
	errs := &errutils.SliceMultiError{Errors: make([]error, 0, len(ps))}

	for _, p := range ps {
		err := p.CheckRequest(req)
		if err == nil {
			return nil
		}
		_ = errs.InsertError(-1, err)
	}

	return errs
}

// NotRequestPolicy is a logical NOT of a RequestPolicy
type NotRequestPolicy struct {
	// Policy is the inverted of this policy
	Policy RequestPolicy

	// Reason is returned if this policy forbids a request
	// If Reason is nil ErrNotPolicy will be returned
	Reason error
}

// CheckRequest performs a logical NOT on the result of the underlying RequestPolicy
func (p NotRequestPolicy) CheckRequest(req *http.Request) error {
	if err := p.Policy.CheckRequest(req); err == nil {
		if p.Reason != nil {
			return p.Reason
		}
		return ErrNotPolicy
	}
	return nil
}
