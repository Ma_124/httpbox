package httpbox

import (
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"testing"
)

func TestRequestPolicyImpl_CheckRequest(t *testing.T) {
	tests := []struct {
		name    string
		policy  *RequestPolicyImpl
		req     *http.Request
		err     error
		wantReq *http.Request
	}{
		{
			name:   "empty policy",
			policy: &RequestPolicyImpl{},
			req: &http.Request{
				Method: "GET",
				URL:    MustParseURL("https://example.com"),
				Proto:  "HTTP/2",
			},
		},
		{
			name: "allow method",
			policy: &RequestPolicyImpl{
				AllowedMethods: map[string]struct{}{"GET": {}},
			},
			req: &http.Request{
				Method: "GET",
				URL:    MustParseURL("https://example.com"),
			},
		},
		{
			name: "forbid method",
			policy: &RequestPolicyImpl{
				AllowedMethods: map[string]struct{}{"POST": {}},
			},
			req: &http.Request{
				Method: "GET",
				URL:    MustParseURL("https://example.com"),
			},
			err: ErrIllegalMethod,
		},
		{
			name:   "allow body",
			policy: &RequestPolicyImpl{},
			req: &http.Request{
				Method: "GET",
				URL:    MustParseURL("https://example.com"),
				Body:   ioutil.NopCloser(strings.NewReader("body text")),
			},
		},
		{
			name: "forbid body",
			policy: &RequestPolicyImpl{
				ForbidBody: true,
			},
			req: &http.Request{
				Method: "GET",
				URL:    MustParseURL("https://example.com"),
				Body:   ioutil.NopCloser(strings.NewReader("body text")),
			},
			err: ErrIllegalBody,
		},
		{
			name: "allow url",
			policy: &RequestPolicyImpl{
				URLPolicy: LiteralURLWhitelistPolicy{`https://example.org`: {}},
			},
			req: &http.Request{
				Method: "GET",
				URL:    MustParseURL("https://example.org"),
			},
		},
		{
			name: "forbid url",
			policy: &RequestPolicyImpl{
				URLPolicy: LiteralURLWhitelistPolicy{`https://www.example.org`: {}},
			},
			req: &http.Request{
				Method: "GET",
				URL:    MustParseURL("https://example.org"),
			},
			err: ErrNotWhitelisted,
		},
		{
			name: "override close (leave it true)",
			policy: &RequestPolicyImpl{
				OverrideClose: LeaveUnmodified,
			},
			req: &http.Request{
				Method: "GET",
				URL:    MustParseURL("https://example.org"),
				Close:  true,
			},
			wantReq: &http.Request{
				Method: "GET",
				URL:    MustParseURL("https://example.org"),
				Close:  true,
			},
		},
		{
			name: "override close (leave it false)",
			policy: &RequestPolicyImpl{
				OverrideClose: LeaveUnmodified,
			},
			req: &http.Request{
				Method: "GET",
				URL:    MustParseURL("https://example.org"),
				Close:  false,
			},
			wantReq: &http.Request{
				Method: "GET",
				URL:    MustParseURL("https://example.org"),
				Close:  false,
			},
		},
		{
			name: "override close (change to true)",
			policy: &RequestPolicyImpl{
				OverrideClose: OverrideWithTrue,
			},
			req: &http.Request{
				Method: "GET",
				URL:    MustParseURL("https://example.org"),
				Close:  false,
			},
			wantReq: &http.Request{
				Method: "GET",
				URL:    MustParseURL("https://example.org"),
				Close:  true,
			},
		},
		{
			name: "override close (change to false)",
			policy: &RequestPolicyImpl{
				OverrideClose: OverrideWithFalse,
			},
			req: &http.Request{
				Method: "GET",
				URL:    MustParseURL("https://example.org"),
				Close:  true,
			},
			wantReq: &http.Request{
				Method: "GET",
				URL:    MustParseURL("https://example.org"),
				Close:  false,
			},
		},
		{
			name:   "host priority",
			policy: &RequestPolicyImpl{},
			req: &http.Request{
				Method: "GET",
				URL:    MustParseURL("https://example.org"),
				Host:   "gitlab.com",
			},
			wantReq: &http.Request{
				Method: "GET",
				URL:    MustParseURL("https://gitlab.com"),
				Host:   "gitlab.com",
			},
		},
		{
			name: "override user agent (leave headers nil)",
			policy: &RequestPolicyImpl{
				OverrideUserAgent: "",
			},
			req: &http.Request{
				Method: "GET",
				URL:    MustParseURL("https://example.org"),
			},
			wantReq: &http.Request{
				Method: "GET",
				URL:    MustParseURL("https://example.org"),
			},
		},
		{
			name: "override user agent (leave)",
			policy: &RequestPolicyImpl{
				OverrideUserAgent: "",
			},
			req: &http.Request{
				Method: "GET",
				URL:    MustParseURL("https://example.org"),
				Header: map[string][]string{"User-Agent": {"Go-http-client/1.1"}},
			},
			wantReq: &http.Request{
				Method: "GET",
				URL:    MustParseURL("https://example.org"),
				Header: map[string][]string{"User-Agent": {"Go-http-client/1.1"}},
			},
		},
		{
			name: "override user agent (set nil to custom)",
			policy: &RequestPolicyImpl{
				OverrideUserAgent: "SomeBot/1.0",
			},
			req: &http.Request{
				Method: "GET",
				URL:    MustParseURL("https://example.org"),
				Header: nil,
			},
			wantReq: &http.Request{
				Method: "GET",
				URL:    MustParseURL("https://example.org"),
				Header: map[string][]string{"User-Agent": {"SomeBot/1.0"}},
			},
		},
		{
			name: "override user agent (set non nil to custom)",
			policy: &RequestPolicyImpl{
				OverrideUserAgent: "SomeBot/1.0",
			},
			req: &http.Request{
				Method: "GET",
				URL:    MustParseURL("https://example.org"),
				Header: map[string][]string{"User-Agent": {"Go-http-client/1.1"}},
			},
			wantReq: &http.Request{
				Method: "GET",
				URL:    MustParseURL("https://example.org"),
				Header: map[string][]string{"User-Agent": {"SomeBot/1.0"}},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := tt.policy.CheckRequest(tt.req)
			if tt.err != nil && err != nil {
				assert.Equal(t, tt.err.Error(), err.Error())
			} else {
				assert.Equal(t, tt.err, err)
			}
			if tt.wantReq != nil {
				assert.Equal(t, tt.wantReq, tt.req)
			}
		})
	}
}

func MustParseURL(s string) *url.URL {
	u, err := url.Parse(s)
	if err != nil {
		panic(err)
	}
	return u
}
