package httpbox

import (
	"gitlab.com/Ma_124/errutils"
	"net/url"
)

// A URLPolicy restricts URLs allowed by CheckURL
type URLPolicy interface {
	// CheckURL checks whether the given *url.URL conforms to this policy
	CheckURL(u *url.URL) error
}

// URLPolicyFunc is an implementation of CheckURL which calls itself
type URLPolicyFunc func(u *url.URL) error

// CheckURL is a wrapper around f
func (f URLPolicyFunc) CheckURL(u *url.URL) error {
	return f(u)
}

// TrueURLPolicy allows all urls
var TrueURLPolicy = URLPolicyFunc(func(u *url.URL) error {
	return nil
})

// FalseURLPolicy forbids all urls
var FalseURLPolicy = URLPolicyFunc(func(u *url.URL) error {
	return ErrNotPolicy
})

// AndURLPolicy is a logical AND of multiple URLPolicies
type AndURLPolicy []URLPolicy

// CheckURL performs a logical AND on the results of the underlying URLPolicies
func (ps AndURLPolicy) CheckURL(u *url.URL) error {
	for _, p := range ps {
		if err := p.CheckURL(u); err != nil {
			return err
		}
	}
	return nil
}

// OrURLPolicy is a logical OR of multiple URLPolicies
type OrURLPolicy []URLPolicy

// CheckURL performs a logical OR on the results of the underlying URLPolicies
func (ps OrURLPolicy) CheckURL(u *url.URL) error {
	errs := &errutils.SliceMultiError{Errors: make([]error, 0, len(ps))}

	for _, p := range ps {
		err := p.CheckURL(u)
		if err == nil {
			return nil
		}
		_ = errs.InsertError(-1, err)
	}

	return errs
}

// NotURLPolicy is a logical NOT of a URLPolicy
type NotURLPolicy struct {
	// Policy is the inverted of this policy
	Policy URLPolicy

	// Reason is returned if this policy forbids an URL
	// If Reason is nil ErrNotPolicy will be returned
	Reason error
}

// CheckURL performs a logical NOT on the result of the underlying URLPolicy
func (p NotURLPolicy) CheckURL(u *url.URL) error {
	if err := p.Policy.CheckURL(u); err == nil {
		if p.Reason != nil {
			return p.Reason
		}
		return ErrNotPolicy
	}
	return nil
}
