package httpbox

import "net/http"

// TripperProxy is a http.RoundTripper that checks whether the requests conform with the Policy and then calls the underlying RoundTripper
type TripperProxy struct {
	// RoundTripper is the wrapped http.RoundTripper
	RoundTripper http.RoundTripper

	// Policy is the RequestPolicyImpl to which all requests must conform
	Policy RequestPolicy
}

// RoundTrip checks whether the requests conform with the Policy and then calls the underlying RoundTripper
func (p *TripperProxy) RoundTrip(req *http.Request) (*http.Response, error) {
	if p.Policy != nil {
		if err := p.Policy.CheckRequest(req); err != nil {
			return nil, err
		}
	}
	return p.RoundTripper.RoundTrip(req)
}
